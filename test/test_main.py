import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
from app.db.database import Base, engine
from app.main import app
from fastapi.testclient import TestClient


Base.metadata.create_all(bind=engine)
client = TestClient(app)
EMAIL = "admin@cidenet.com.co"
LOGIN = "/auth/login"
EMPLOYEE_ID = "/employee/fe7bd35e-a0e1-4454-b323-8a6b6911ca91"


def create_database():
    """
    It creates a database
    """
    Base.metadata.create_all(bind=engine)


def test_user_signup():
    """
    It tests the user signup endpoint by sending a POST request to the endpoint with a user object as
    the payload
    """
    user = {
        "user_id": "58e516fc-56b1-4f80-8c9a-e2b78e81fe7e",
        "email": EMAIL,
        "first_name": "Hernando",
        "last_name": "Escobar",
        "password": "12345678",
        "user_type": "Admin"
    }
    response = client.post('/auth/signup', json=user)
    assert response.status_code == 201
    assert response.json()


def test_user_existing_signup():
    """
    The above function tests if a user can sign up with an existing email.
    """
    user = {
        "user_id": "58e516fc-56b1-4f80-8c9a-e2b78e81fe7e",
        "email": EMAIL,
        "first_name": "Hernando",
        "last_name": "Escobar",
        "password": "12345678",
        "user_type": "Admin"
    }
    response = client.post('/auth/signup', json=user)
    assert response.status_code == 400
    assert response.json() == {"detail": "Email already registered"}


def test_user_login():
    """
    It tests that a user can login with the correct credentials
    """
    user = {
        "username": EMAIL,
        "password": "12345678",
    }
    response = client.post(LOGIN, data=user)
    assert response.status_code == 200
    assert response.json()


def test_employee_create():
    """
    It creates a user, logs in, and creates an employee
    """
    user = {
        "username": EMAIL,
        "password": "12345678",
    }
    response = client.post(LOGIN, data=user)
    assert response.status_code == 200
    assert response.json()
    headers = {
        "Authorization": f"Bearer {response.json()['access_token']}"
    }
    employee = {
        "employee_id": "fe7bd35e-a0e1-4454-b323-8a6b6911ca91",
        "first_name": "HERNANDO",
        "last_name": "ESCOBARB",
        "additional_names": "GUZMAN ",
        "country_job": "Colombia",
        "type_id": "Cédula de Ciudadanía",
        "id_number": "l18sj334112221",
        "date_entered": "01/01/2023 10:00:00",
        "area": "Operación"
    }
    response = client.post('/employee/create', json=employee, headers=headers)
    assert response.status_code == 201
    assert response.json()


def test_employee_get_by_id():
    """
    The above function tests the get employee by id endpoint.
    """
    user = {
        "username": EMAIL,
        "password": "12345678",
    }
    response = client.post(LOGIN, data=user)
    assert response.status_code == 200
    assert response.json()
    headers = {
        "Authorization": f"Bearer {response.json()['access_token']}"
    }
    response = client.get(EMPLOYEE_ID, headers=headers)
    assert response.status_code == 200
    assert response.json()


def test_employee_update():
    """
    The above function updates the employee with the given id.
    """
    user = {
        "username": EMAIL,
        "password": "12345678",
    }
    response = client.post(LOGIN, data=user)
    assert response.status_code == 200
    assert response.json()
    headers = {
        "Authorization": f"Bearer {response.json()['access_token']}"
    }
    employee = {
        "employee_id": None,
        "first_name": "HERNANDO",
        "last_name": "ESCOBARGUZ",
        "additional_names": "GUZMAN ",
        "country_job": "Colombia",
        "type_id": "Cédula de Ciudadanía",
        "id_number": "l18sj334112221",
        "date_entered": "01/01/2023 10:00:00",
        "area": "Financiera"
    }
    response = client.patch(EMPLOYEE_ID, json=employee, headers=headers)
    assert response.status_code == 200
    assert response.json()


def test_employee_delete():
    """
    It deletes the employee with the given id.
    """
    user = {
        "username": EMAIL,
        "password": "12345678",
    }
    response = client.post(LOGIN, data=user)
    assert response.status_code == 200
    assert response.json()
    headers = {
        "Authorization": f"Bearer {response.json()['access_token']}"
    }
    response = client.delete(EMPLOYEE_ID, headers=headers)
    assert response.status_code == 200
    assert response.json()


# def test_delete_database():
#     """
#     It deletes the database
#     """
#     db_path = os.path.join(os.path.dirname(__file__), '../local.db')
#     os.remove(db_path)
