from fastapi import FastAPI
from app.db.database import Base, engine
from app.auth.routers import auth
from app.user.routers import user
from app.employee.routers import employee
from fastapi_pagination import add_pagination
from fastapi.middleware.cors import CORSMiddleware

origins = [
    "http://localhost",
    "http://localhost:8000",
]
description = """
Cidenet API  test. 🚀

# Prueba Técnica Backend Python
## Employees

"""


def create_database():
    Base.metadata.create_all(bind=engine)


create_database()


app = FastAPI(
    title="Cidenet",
    description=description,
    version="0.0.1",
    terms_of_service="https://leeward-iberis-b63.notion.site/Cidenet-Test-92911f57715f4fa6b36b6e17d3756f82",
    contact={
        "name": "Hernando Escobar",
        "url": "https://gitlab.com/SerraNando/cidenet-test",
        "email": "hernandoescobar23@gmail.com",
    },
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

add_pagination(app)
app.include_router(auth.router)
app.include_router(user.router)
app.include_router(employee.router)


