from fastapi import BackgroundTasks, APIRouter, Depends, status, Body, HTTPException
from sqlalchemy.orm import Session
from app.auth.schema.auth import TokenResponse
from app.db.database import get_db
from app.auth.repository import auth
from fastapi.security import OAuth2PasswordRequestForm
from app.user.schema.user import UserCreate, UserResponse
from app.user.repository import user
from app.utils.monitor import log_session_service


router = APIRouter(
    prefix="/auth",
    tags=["Auth"]
)

@router.post(
    '/signup',
    status_code=status.HTTP_201_CREATED,
    response_model=UserResponse,
    summary="Register a User",
    include_in_schema= False
)
def signup(user_body: UserCreate = Body(...), db: Session = Depends(get_db)):
    """
    Register User

    This path operation Register a user in the app

    Parameters:
    Request body parameter
    - **user**: UserCreate

    Returns a json with the basic user information:
    - **user**: UserResponse

    """
    db_user = user.get_user_by_email(db, email=user_body.email)
    if db_user:
        raise HTTPException(
            status_code=400, detail=f"""Email already registered""")
    return user.create_user(db, user_body)


@router.post(
    '/login',
    status_code=status.HTTP_200_OK,
    response_model=TokenResponse,
    summary="Login a User",

)
async def login(background_tasks: BackgroundTasks, user: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """
    Login User

    This path operation Login a user in the app

    Parameters:
        - Request body parameter
            - user: OAuth2PasswordRequestForm

    Returns a json with the basic user information:
        -  user: TokenResponse

    """
    auth_token = auth.auth_user(user, db)
    background_tasks.add_task(log_session_service, db,
                              auth_token['access_token'])
    return auth_token
