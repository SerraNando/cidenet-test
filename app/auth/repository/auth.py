from sqlalchemy.orm import Session
from app.user import models
from fastapi import HTTPException, status
from app.utils.hashing import Hash
from app.utils.token import create_access_token

RESPONSE = """Incorrect user or password please retry!"""


def auth_user(user_request, db: Session):
    """
    It takes a user_request and a db session, then it queries the database for a user with the email
    address of the user_request.username. If it finds a user, it checks if the password is correct. If
    it is, it creates an access token and returns it
    
    :param user_request: The user request object
    :param db: Session = Depends(get_db)
    :type db: Session
    :return: A dictionary with the access_token and token_type
    """
    user = db.query(models.User).filter(
        models.User.email == user_request.username).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=RESPONSE
        )
    if not Hash.verify_password(user_request.password, user.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=RESPONSE
        )
    access_token = create_access_token(
        data={"sub": user.email, "user_type": user.user_type}
    )
    return {"access_token": access_token, "token_type": "bearer"}
