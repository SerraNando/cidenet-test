
from pydantic import BaseModel
from typing import Union


class Login(BaseModel):
    email: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Union[str, None] = None
    user_type: Union[str, None] = None


class TokenResponse(BaseModel):
    access_token: str
    token_type: str
