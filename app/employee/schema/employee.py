# Pydantic
from pydantic import BaseModel, validator
from pydantic import Field
from typing import Optional
from app.utils.custom_validator import names, additional_names, date_entered
from enum import Enum


class Country(str, Enum):
    USA = 'USA'
    COLOMBIA = 'Colombia'


class TypeId(str, Enum):
    IDENTITY_CARD = 'Cédula de Ciudadanía'
    FOREIGNER_ID = 'Cédula de Extranjería'
    PASSPORT = 'Pasaporte'
    SPECIAL_PERMISSION = 'Permiso Especial'


class Area(str, Enum):
    ADMINISTRATION = 'Administración'
    FINANCIAL = 'Financiera'
    BUYING = 'Compras'
    INFRASTRUCTURE = 'Infraestructura'
    OPERATION = 'Operación'
    HUMAN_RESOURCES = 'Talento Humano'
    VARIOUS_SERVICE = 'Servicios Varios'
    OTHERS = 'Otros'


class EmployeeBase(BaseModel):
    employee_id: Optional[str] = Field(

        example='fe7bd35e-a0e1-4454-b323-8a6b6911ca90'

    )
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=20,
        example='HERNANDO'

    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=20,
        example='ESCOBAR'

    )
    additional_names: Optional[str] = Field(
        min_length=1,
        max_length=50,
        example='GUZMAN '

    )
    country_job: Country = Field(
        ...,
        example=Country.COLOMBIA

    )
    type_id: TypeId = Field(
        ...,
        example=TypeId.IDENTITY_CARD

    )
    id_number: str = Field(
        ...,
        min_length=1,
        max_length=20,
        example='l18sj33411222'

    )

    date_entered: Optional[str] = Field(

        example='01/01/2023 10:00:00'

    )
    area: Area = Field(
        ...,
        example=Area.OPERATION

    )

    @validator('first_name')
    def validation_first_name(cls, value):
        """
        It takes a string, and if it's not a string, it raises a TypeError. If it is a string, it checks to
        see if it's a valid name. If it's not a valid name, it raises a ValueError. If it is a valid name,
        it returns the string

        :param cls: The class that the method is bound to
        :param value: The value of the field to be validated
        """
        return names(value)

    @validator('last_name')
    def validation_last_name(cls, value):
        """
        It takes a string, and if it's not a string, it raises a TypeError. If it is a string, it checks
        to see if it's a valid name. If it's not a valid name, it raises a ValueError. If it is a valid
        name, it returns the string
        :param cls: The class that the method is bound to
        :param value: The value of the field to be validated
        """
        return names(value)

    @validator('additional_names')
    def validation_additional_names(cls, value):
        """
        :param cls: The class that the decorator is being applied to
        :param value: The value of the field to be validated
        """
        if value is None:
            return value
        else:
            return additional_names(value)

    @validator('id_number')
    def validation_id_numbers(cls, value):
        """
        The function takes a string as an argument and returns the string if it contains only letters of the
        alphabet A-Z or numbers 0-9.

        :param cls: The class that the method is bound to
        :param value: The value of the field being validated
        :return: The value of the variable 'value'
        """
        assert value.isalnum(), 'must contain only letters of the alphabet A-Z or numbers 0-9'
        return value

    @validator('date_entered')
    def validation_date_entered(cls, value):
        """
        If the value is None, return the value. Otherwise, return the value after it has been run through
        the date_entered function.

        :param cls: The class that the method is bound to
        :param value: The value to be validated
        :return: The value of the date_entered function.
        """
        if value is None:
            return value
        else:
            return date_entered(value)

    class Config:
        orm_mode = True


class EmployeeResponse(EmployeeBase):
    employee_id: str = Field(

        example='fe7bd35e-a0e1-4454-b323-8a6b6911ca90'

    )
    email: str = Field(
        example='user.demo@cidenet.com.co'
    )
    date_entered: str = Field(

        example='01/01/2023 10:00:00'

    )
    area: Area = Field(
        example=Area.OPERATION

    )

    class Config:
        orm_mode = True
