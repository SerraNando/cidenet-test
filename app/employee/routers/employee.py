from fastapi import BackgroundTasks, APIRouter, status, Body, Depends, Path
from app.employee.schema.employee import EmployeeBase, EmployeeResponse
from app.employee.repository import employee
from sqlalchemy.orm import Session
from app.db.database import get_db
from app.utils.monitor import monitor_service
from app.utils.oauth import get_current_user
from app.user.schema.user import User
from app.monitor.schema.monitor import Monitor
from fastapi_pagination import Page, paginate


router = APIRouter(
    prefix="/employee",
    tags=["Employees"],
    responses={404: {"description": "Not found"}},
)


@router.post(
    '/create',
    status_code=status.HTTP_201_CREATED,
    response_model=EmployeeResponse,
    summary="Create Employee",
)
def create(
    background_tasks: BackgroundTasks,
    employee_body: EmployeeBase = Body(...),
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user)
):
    """
    Create Employee

    This path operation Create Employee in the app

    Parameters:
    Request body parameter
    - **employee**: EmployeeBase

    Returns a json with the basic employee information:
    - **employee**: EmployeeResponse

    """

    response = employee.create_employee(db, employee_body, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='POST',
            path="/employee/create",
            status="successful",
            error=status.HTTP_201_CREATED
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='POST',
            path="/employee/create",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return response["response"]


@router.get(
    '/',
    response_model=Page[EmployeeResponse],
    status_code=status.HTTP_200_OK,
    summary='Get Employees'
)
def get_employees(
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user)
):
    """
    This path operation shows all employee in the app:

    Returns a json with the basic employee information:
    - **employee**: [EmployeeResponse]
    """
    response = employee.get_employees(db, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path="/employee/",
            status="successful",
            error=status.HTTP_200_OK
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path="/employee/",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return paginate(response["response"])


@router.get(
    '/{employee_id}',
    response_model=EmployeeResponse,
    status_code=status.HTTP_200_OK,
    summary='Get Employee by ID'
)
def get_employee(
    background_tasks: BackgroundTasks,
    employee_id: str = Path(
        ...,
        example="fe7bd35e-a0e1-4454-b323-8a6b6911ca90"
    ),
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user)

):
    """
    This path operation  get employee in the app:
    Parameters:
    - Request path parameter
    - **employee_id**: str


    Returns a json with the basic employee information:
    - **employee**: EmployeeResponse
    """
    response = employee.get_employee(employee_id, db, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/{employee_id}",
            status="successful",
            error=status.HTTP_200_OK
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/{employee_id}",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return response["response"]


@router.get(
    '/search/{search}',
    response_model=Page[EmployeeResponse],
    status_code=status.HTTP_200_OK,
    summary='Get Employees for search'
)
def get_employees_search(
    background_tasks: BackgroundTasks,
    search: str = Path(
        ...,
        example="Colombia"
    ),
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user)

):
    """
    This path operation  get employees for search in the app:
    Parameters:
    - Request path parameter
    - **search**: str

    Returns a json with the basic employee information:
    - **employee**: [EmployeeResponse]
    """
    response = employee.get_employee_search(search, db, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/search/{search}",
            status="successful",
            error=status.HTTP_200_OK
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/search/{search}",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return paginate(response["response"])


@router.patch(
    '/{employee_id}',
    status_code=status.HTTP_200_OK,
    summary='Update Employee'
)
def update_employee(
    background_tasks: BackgroundTasks,
    employee_id: str = Path(
        ...,
        example="fe7bd35e-a0e1-4454-b323-8a6b6911ca90"
    ),
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user),
    employee_body: EmployeeBase = Body(...),

):
    """
    This path operation  update employee in the app:
    Parameters:
    - Request path parameter
    - **employee_id**: str
    Request body parameter
    - **employee**: EmployeeBase


    Returns a json with the basic employee information:
    - **employee**: EmployeeResponse
    """
    response = employee.update_employee(
        employee_body, employee_id, db, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='PATCH',
            path=f"/employee/{employee_id}",
            status="successful",
            error=status.HTTP_200_OK
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='PATCH',
            path=f"/employee/{employee_id}",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return response["response"]


@router.delete(
    '/{employee_id}',
    status_code=status.HTTP_200_OK,
    summary='Delete Employee'
)
def delete_employee(
    background_tasks: BackgroundTasks,
    employee_id: str = Path(
        ...,
        example="fe7bd35e-a0e1-4454-b323-8a6b6911ca90"
    ),
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user),

):
    """
    This path operation  delete employee in the app:
    Parameters:
    - Request path parameter
    - **employee_id**: str

    Returns a json with the basic employee information:
    - **employee**: EmployeeResponse
    """
    response = employee.delete_employee(employee_id, db, current_user)
    if "status" in response:
        data = Monitor(
            email=current_user.email,
            method='DELETE',
            path=f"/employee/{employee_id}",
            status="successful",
            error=status.HTTP_200_OK
        )
        background_tasks.add_task(monitor_service, db, data)
    else:
        data = Monitor(
            email=current_user.email,
            method='DELETE',
            path=f"/employee/{employee_id}",
            status="failed",
            error=response
        )
        background_tasks.add_task(monitor_service, db, data, False
                                  )
    return response["response"]


