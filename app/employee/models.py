
from app.db.database import Base
from sqlalchemy import Column, String, DateTime
from datetime import datetime


class Employee(Base):
    __tablename__ = "employees"
    employee_id = Column(String, primary_key=True, index=True)
    first_name = Column(String, index=True)
    last_name = Column(String, index=True)
    additional_names = Column(String, index=True)
    country_job = Column(String, index=True)
    type_id = Column(String, index=True)
    id_number=Column(String,unique=True, index=True)
    email = Column(String, unique=True, index=True)
    date_entered = Column(String)
    area = Column(String)
    status= Column(String, default='Enabled')
    created_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
