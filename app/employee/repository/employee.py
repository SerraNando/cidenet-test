
from app.employee import models
from fastapi import HTTPException, status
from app.employee.schema.employee import EmployeeBase
from sqlalchemy.orm import Session
import uuid
from sqlalchemy import or_, update, delete
from app.monitor.schema.monitor import Monitor
from app.user.models import User
from app.utils.monitor import monitor_service


def email_counters(country: str, first_name: str, last_name: str, db: Session):
    """
    If the country is USA, then return the email address with the country extension.
    If the country is Colombia, then return the email address with the country extension.

    :param country: str
    :type country: str
    :param first_name: str
    :type first_name: str
    :param last_name: str
    :type last_name: str
    :param db: Session = Depends(get_db)
    :type db: Session
    :return: A string
    """
    if 'USA' in country:
        email = f'{first_name}.{last_name}'
        count = exist_employee(db, email, country)
        if count == 1:
            return f'{first_name}.{last_name}.{count}@cidenet.com.us'
        elif count > 1:
            return f'{first_name}.{last_name}.{count}@cidenet.com.us'
        else:
            return f'{first_name}.{last_name}@cidenet.com.us'
    if 'Colombia' in country:
        email = f'{first_name}.{last_name}'
        count = exist_employee(db, email, country)
        if count == 1:
            return f'{first_name}.{last_name}.{count}@cidenet.com.co'
        elif count > 1:
            return f'{first_name}.{last_name}.{count}@cidenet.com.co'
        else:
            return f'{first_name}.{last_name}@cidenet.com.co'


def create_employee(db: Session, employee: EmployeeBase, current_user: User):
    """
    It creates an employee in the database

    :param db: Session = Depends(get_db)
    :type db: Session
    :param employee: EmployeeBase = Depends(get_current_employee)
    :type employee: EmployeeBase
    :param current_user: User
    :type current_user: User
    :return: A dictionary with two keys: response and status.
    """
    employee = employee.dict()
    email = email_counters(employee['country_job'], employee['first_name'].lower(
    ), employee['last_name'].lower(), db)
    if employee['employee_id'] is None:
        employee['employee_id'] = str(uuid.uuid4())
    try:
        db_employee = models.Employee(
            employee_id=employee['employee_id'],
            first_name=employee['first_name'],
            last_name=employee['last_name'],
            additional_names=employee['additional_names'],
            country_job=employee['country_job'],
            type_id=employee['type_id'],
            id_number=employee['id_number'],
            email=email,
            date_entered=employee['date_entered'],
            area=employee['area'],
        )
        db.add(db_employee)
        db.commit()
        db.refresh(db_employee)
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='POST',
            path="/employee/create",
            status="failed",
            error=status.HTTP_409_CONFLICT
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error creating employee {error}"""
        )
    return {
        "response": db_employee,
        "status": True}


def exist_employee(db: Session, email: str, country_job: str):
    """
    It returns the number of employees in the database whose email address starts with the given email
    address and whose country_job is the given country_job

    :param db: Session, email: str, country_job: str
    :type db: Session
    :param email: str
    :type email: str
    :param country_job: str
    :type country_job: str
    :return: The number of employees that have the same email and country_job.
    """
    exists = db.query(models.Employee).filter(models.Employee.country_job ==
                                              country_job).filter(models.Employee.email.like(email+'%')).count()
    return exists


def get_employees(db: Session, current_user: User):
    """
    It queries the database for all employees and returns the data

    :param db: Session = Depends(get_db)
    :type db: Session
    :param current_user: User
    :type current_user: User
    :return: A dictionary with two keys: response and status.
    """
    try:
        data = db.query(models.Employee).all()
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='GET',
            path="/employee/",
            status="failed",
            error=status.HTTP_409_CONFLICT
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error creating employee {error}"""
        )
    return {
        "response": data,
        "status": True}


def get_employee(employee_id: str, db: Session, current_user: User):
    """
    It gets an employee from the database, if it doesn't exist, it logs the error and raises an
    exception

    :param employee_id: str,
    :type employee_id: str
    :param db: Session = Depends(get_db)
    :type db: Session
    :param current_user: User
    :type current_user: User
    :return: A dictionary with two keys, response and status.
    """
    try:
        data = db.query(models.Employee).filter(
            models.Employee.employee_id == employee_id).first()
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/{employee_id}",
            status="failed",
            error=status.HTTP_404_NOT_FOUND
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The employee with the id does not exist {employee_id}, {error}"

        )
    return {
        "response": data,
        "status": True}


def get_employee_search(search: str, db: Session, current_user: User):
    """
    It takes a search string, a database session, and a current user object and returns a dictionary
    with a response and a status

    :param search: str, db: Session, current_user: User
    :type search: str
    :param db: Session = Depends(get_db)
    :type db: Session
    :param current_user: User
    :type current_user: User
    :return: A list of dictionaries
    """
    try:
        columns = [models.Employee.first_name, models.Employee.last_name, models.Employee.additional_names,
                   models.Employee.area, models.Employee.country_job, models.Employee.type_id, models.Employee.status, models.Employee.id_number]
        expression = or_(*[col.ilike(f"%{search}%") for col in columns])
        data = db.query(models.Employee).filter(expression).all()
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='GET',
            path=f"/employee/search/{search}",
            status="failed",
            error=status.HTTP_404_NOT_FOUND
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The employee {search}, {error}"
        )
    return {
        "response": data,
        "status": True}


def update_employee(employee: EmployeeBase, employee_id: str, db: Session, current_user: User):
    """
    It updates the employee's details in the database

    :param employee: EmployeeBase
    :type employee: EmployeeBase
    :param employee_id: str
    :type employee_id: str
    :param db: Session = Depends(get_db)
    :type db: Session
    :param current_user: User
    :type current_user: User
    :return: a dictionary with the response and status.
    """
    employee = employee.dict()
    if employee['employee_id'] is None:
        employee['employee_id'] = employee_id
    try:
        data = db.query(models.Employee).filter(
            models.Employee.employee_id == employee_id).first()
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='PATCH',
            path=f"/employee/{employee_id}",
            status="failed",
            error=status.HTTP_404_NOT_FOUND
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The employee with the id does not exist {employee_id}, {error}"

        )
    if data.first_name == employee['first_name'] and data.last_name == employee['last_name']:
        if data.country_job == employee['country_job']:
            try:
                db.execute(update(models.Employee).where(
                    models.Employee.employee_id == employee_id).values(employee))
                db.commit()
                return {
                    "response":  f"""Employee updated successfully!""",
                    "status": True}
            except Exception as error:
                db.rollback()
                data = Monitor(
                    email=current_user.email,
                    method='PATCH',
                    path=f"/employee/{employee_id}",
                    status="failed",
                    error=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
                monitor_service(db, data)
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Error {employee_id}, {error}")
        else:
            email = email_counters(employee['country_job'], employee['first_name'].lower(
            ), employee['last_name'].lower(), db)

            employee['email'] = email
            try:
                db.execute(update(models.Employee).where(
                    models.Employee.employee_id == employee_id).values(employee))
                db.commit()
                return {
                    "response":  f"""Employee updated successfully!""",
                    "status": True}
            except Exception as error:
                db.rollback()
                data = Monitor(
                    email=current_user.email,
                    method='PATCH',
                    path=f"/employee/{employee_id}",
                    status="failed",
                    error=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
    else:
        email = email_counters(employee['country_job'], employee['first_name'].lower(
        ), employee['last_name'].lower(), db)

        employee['email'] = email
        db.execute(update(models.Employee).where(
            models.Employee.employee_id == employee_id).values(employee))
        db.commit()
        return {
            "response":  f"""Employee updated successfully!""",
            "status": True}


def delete_employee(employee_id: str, db: Session, current_user: User):
    """
    It deletes an employee from the database

    :param employee_id: str, db: Session, current_user: User
    :type employee_id: str
    :param db: Session = Depends(get_db)
    :type db: Session
    :param current_user: User
    :type current_user: User
    :return: The return is a dictionary with two keys, response and status.
    """
    try:
        data = db.query(models.Employee).filter(
            models.Employee.employee_id == employee_id).first()
    except Exception as error:
        db.rollback()
        data = Monitor(
            email=current_user.email,
            method='DELETE',
            path=f"/employee/{employee_id}",
            status="failed",
            error=status.HTTP_404_NOT_FOUND
        )
        monitor_service(db, data)
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The user with the id does not exist {employee_id}, {error}"

        )
    db.execute(delete(models.Employee).where(
        models.Employee.employee_id == employee_id))
    return {
        "response":  f"""Employee delete successfully!""",
        "status": True}
