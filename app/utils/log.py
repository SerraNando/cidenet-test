

def write_notification(function: str, error=""):
	"""
	It opens a file called log.txt, writes a string to it, and then closes the file
	
	:param function: the name of the function that failed
	:type function: str
	:param error: the error message
	"""
	with open("log.txt", mode="w") as email_file:
		content = f"error for {function}: {error}"
		email_file.write(content)
