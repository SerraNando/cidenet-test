from datetime import datetime, timedelta
from jose import JWTError, jwt
from app.auth.schema.auth import TokenData
from core.config import settings
from app.user.repository import user


SECRET_KEY = settings.SECRET_KEY
ALGORITHM = settings.ALGORITHM
ACCESS_TOKEN_EXPIRE_MINUTES = settings.ACCESS_TOKEN_EXPIRE_MINUTES


def create_access_token(data: dict):
    """
    It takes a dictionary of data, adds an expiry date to it, and then encodes it using a secret key and
    an algorithm
    :param data: dict = {"sub": "1234567890"}
    :type data: dict
    :return: A JWT token
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_token(token: str, credentials_exception, db):
    """
    If the token is valid, return the token data. If not, raise an exception.
    
    :param token: str = the token that is being verified
    :type token: str
    :param credentials_exception: HTTPException
    :param db: sqlalchemy.orm.session.Session
    :return: A TokenData object
    """
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        user_type: str = payload.get("user_type")
        if email is None:
            raise credentials_exception
        db_user = user.get_user_by_email(db, email=email)
        token_data = TokenData()

        if db_user.email == email:
            token_data.email = email
        else:
            raise credentials_exception
        if db_user.user_type == user_type:
            token_data.user_type = user_type
        else:
            raise credentials_exception

        return token_data
    except JWTError:
        raise credentials_exception


def verify_basic_token(token: str):
    """
    It takes a token, decodes it, and returns a TokenData object
    :param token: str = the token that you want to verify
    :type token: str
    :return: A TokenData object
    """
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    email: str = payload.get("sub")
    user_type: str = payload.get("user_type")

    token_data = TokenData(
        email=email, user_type=user_type)
    return token_data
