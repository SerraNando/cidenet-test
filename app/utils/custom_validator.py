from app.utils.regex import regex_search, space_regex, check_alphabet_and_spaces
from datetime import datetime, timedelta



def names(value):
    """
    "The value must be a string of letters in uppercase without accents or Ñ."
    
    The first assert statement checks that the value is a string of letters. The second assert statement
    checks that the value is in uppercase. The third assert statement checks that the value does not
    contain Ñ or accents
    
    :param value: The value that is being validated
    :return: The value of the variable.
    """
    assert check_alphabet_and_spaces(value), 'must contain only letters of the alphabet A-Z'
    assert value.isupper(), 'must be written in UpperCase'
    assert regex_search(value), 'should not have Ñ or accents'
    return value


def additional_names(value):
    """
    It takes a string as an argument and returns the same string if it meets the following conditions:
    
    - It must contain only letters of the alphabet A-Z
    - It must be written in UpperCase
    - It should not have Ñ or accents
    - It should not count spaces between names
    
    :param value: The value that is being passed to the function
    :return: The value of the variable is being returned.
    """
    assert space_regex(value), 'should not count spaces between names'
    assert value.isupper(), 'must be written in UpperCase'
    assert regex_search(value), 'should not have Ñ or accents'
    assert check_alphabet_and_spaces(value), 'must contain only letters of the alphabet A-Z'
    return value


def validate_date(date):
    _date_entered = datetime.strptime(date, "%d/%m/%Y %H:%M:%S")
    now = datetime.now()
    if _date_entered > now:
        return False
    one_month_ago = now - timedelta(days=30)
    if _date_entered > one_month_ago:
        return True
    return False

def date_entered(value):
    assert validate_date(value), 'May not exceed the current date, but may be up to one month less'
    return value
