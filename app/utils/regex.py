import re


def regex_search(value):
    """
    It takes a string as an argument and returns True if the string contains any special characters, and
    False if it doesn't
    :param value: The value to be checked
    :return: A boolean value.
    """
    regex = re.compile(r'[\u00F1\u00D1|\u00E0-\u00FF|\u00C0-\u00FF]')
    result_regex = regex.search(value)
    if result_regex:
        return False
    else:
        return True


def space_regex(value):
   return bool(re.search(r"\s", value))


def check_alphabet_and_spaces(string):
    result = re.fullmatch(r'[A-Za-z ]+', string)
    if result:
        return True
    else:
        return False
