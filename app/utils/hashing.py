from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# The hash_password method takes a plain text password and returns a hashed password

class Hash:
    def hash_password(password):
        """
        It takes a password as input, and returns a hash of that password
        
        :param password: The password to be hashed
        :return: The hash of the password.
        """
        return pwd_context.hash(password)

    def verify_password(plain_password, hashed_password):
        """
        It takes a plain text password and a hashed password and returns True if the plain text password
        matches the hashed password
        
        :param plain_password: The password in plain text
        :param hashed_password: The hashed password that was stored in the database
        :return: the hashed password.
        """
        return pwd_context.verify(plain_password, hashed_password)
