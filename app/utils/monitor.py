from sqlalchemy.orm import Session
from app.monitor.repository.monitor import create_log_service, create_monitor_services
from app.monitor.schema.monitor import Log, Monitor
from app.utils.token import verify_basic_token


def log_session_service(db_session:Session, token):
    """
    It creates a log entry in the database.

    :param db_session: This is the database session object
    :type db_session: Session
    :param token: The token that was passed in the header of the request
    """
    token_result =verify_basic_token(token)

    log=Log(email=token_result.email)
    create_log_service(db_session, log)

def monitor_service(db_session:Session,data:Monitor):
    """
    > This function creates a new monitor service in the database

    :param db_session: This is the database session object
    :type db_session: Session
    :param data: This is the data that is passed to the function
    :type data: Monitor
    """
    create_monitor_services(db_session,data)