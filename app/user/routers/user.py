from fastapi import APIRouter, status, Depends
from app.user.schema.user import UserResponse, User
from app.user.repository import user
from sqlalchemy.orm import Session
from app.db.database import get_db
from app.utils.oauth import get_current_user
from fastapi_pagination import Page, paginate


router = APIRouter(
    prefix="/user",
    tags=["Users"],
    responses={404: {"description": "Not found"}},
)


@router.get(
    '/',
    response_model=Page[UserResponse],
    status_code=status.HTTP_200_OK,
    summary='Get Users'
)
def show_all_users(
        db: Session = Depends(get_db),
        current_user: User = Depends(get_current_user)):
    """
    This path operation shows all users in the app:

    Returns a json list with all users in the app, with the following keys:
    - **user_id** : str
    - **email** : Emailstr
    - **first_name** : str
    - **last_name** :  str
    """
    data = user.get_users(db, current_user)
    return paginate(data)
