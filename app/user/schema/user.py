# Pydantic
from uuid import UUID
from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import Field


# User Model

class UserBase(BaseModel):
    user_id: str = Field(...)
    email: EmailStr = Field(
        ...,
        example='test@dominio.com'
    )

    class Config:
        orm_mode = True


class User(UserBase):  # Schema
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example='Hernando'

    )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example='Escobar'

    )
    



class UserCreate(User):
    password: str = Field(
        ...,
        min_length=8,
        max_length=60,
        example='12345678'

    )
    user_type: str = Field(example='Admin')
    


class UserResponse(BaseModel):
    user_id: str
    email: str
    first_name: str
    last_name: str

    class Config():
        orm_mode = True




