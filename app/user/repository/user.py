from app.user import models
from fastapi import HTTPException, status
from app.user.schema.user import UserCreate
from sqlalchemy.orm import Session
from app.utils.hashing import Hash


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, current_user):
    if 'Admin' in current_user.user_type:
        data = db.query(models.User).all()
        return data
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"""You do not have permission to view this information!"""
        )


def create_user(db: Session, user: UserCreate):
    user = user.dict()
    try:
        db_user = models.User(
            user_id=user['user_id'],
            email=user['email'],
            first_name=user['first_name'],
            last_name=user['last_name'],
            password=Hash.hash_password(user["password"]),
            user_type=user['user_type']

        )
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error creating user {e}"""
        )
    return db_user


def update_user(user_id, update_user, db: Session):
    user = db.query(models.User).filter(models.User.user_id == user_id)
    if not user.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The user with the id does not exist {user_id}"
        )
    user.update(update_user.dict(exclude_unset=True))
    db.commit()
    return {"detail": f"""User updated successfully!"""}



