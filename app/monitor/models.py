
from app.db.database import Base
from sqlalchemy import Column, String, DateTime
from datetime import datetime


class Monitor(Base):
    __tablename__ = "monitor_services"
    monitor_service_id = Column(String, primary_key=True, index=True)
    email = Column(String, index=True)
    method = Column(String)
    path = Column(String)
    status = Column(String)
    error = Column(String)
    created_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)


class Log(Base):
    __tablename__ = "log_sessions"
    log_session_id = Column(String, primary_key=True, index=True)
    email = Column(String, index=True)
    created_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
