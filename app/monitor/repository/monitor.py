from sqlalchemy.orm import Session
from app.monitor.schema.monitor import Log, Monitor
from app.monitor import models
import uuid

from app.utils.log import write_notification


def create_log_service(db: Session, log: Log):
    log = log.dict()
    try:
        db_log = models.Log(
            log_session_id=str(uuid.uuid4()),
            email=log['email'],

        )
        db.add(db_log)
        db.commit()
        db.refresh(db_log)
    except Exception as e:
        write_notification('create_log_service', e)

    return True


def create_monitor_services(db: Session, monitor: Monitor):
    monitor = monitor.dict()
    try:
        db_monitor = models.Monitor(
            monitor_service_id=str(uuid.uuid4()),
            email=monitor['email'],
            method=monitor['method'],
            path=monitor['path'],
            status=monitor['status'],
            error=monitor['error'],

        )
        db.add(db_monitor)
        db.commit()
        db.refresh(db_monitor)
    except Exception as e:
        write_notification('create_monitor_services', e)

    return True
