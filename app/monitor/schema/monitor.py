from pydantic import Field, EmailStr, BaseModel


class Log(BaseModel):
    email: EmailStr = Field(...)

    class Config:
        orm_mode = True


class Monitor(BaseModel):
    email: EmailStr = Field(...)
    method: str = Field(...)
    path: str = Field(...)
    status: str = Field(...)
    error: str = Field(...)

    class Config:
        orm_mode = True
